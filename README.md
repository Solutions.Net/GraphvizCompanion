# GraphvizCompanion

Just a library to embed graphviz in a C# project.
It allows user to display svg, in live, from a dot description

This tiny program is just to help me to debug some R&D code I was working on recently at work 
(dynamic rewriting of C# code to build big BI decision tree).
Graphviz was definitely the right tool...
I previously used https://edotor.net/ that is an excellent tool but with some frustrating limits.


**Limits i don't have with this project:**

- This project is offline
- Zoom with a big factor (x1000).
- Right click on display to get the svg "code" or to get the full standalone html page (including some javascript libraries)
- Handle complex graph resulting in big image and where dot needs more memory 
  (edotor's version of graphivz often returns an error with a max memory size limitation, same with other tool online)
- You can update the graphviz library just by replacing the folder "lib"
 (In case you want to recompile library to get some optionnal support, for exmeple delaunay triangulation)


**However, missing features:**

- UX for errors in your dot text (highlight the line where error is)
==> I will probably use avalon editor next time (to get at least line indexes)
- Cannot export it to png or other format from UI. But svg ca be converted by using online tool anyway
- Ability to run it elsewhere than windows (I can't wait for an official "go live" of Net 5.0 though)


**How to compile?**

- Just install Visual Studio express (free)
- Open (Double click) the ".sln" file 
- Compile project & Run it with key F5

![](images/startup_state.png)