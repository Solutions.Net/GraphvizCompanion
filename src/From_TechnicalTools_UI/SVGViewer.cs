﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;


namespace TechnicalTools.UI.DX.Controls
{
    // TODO: Test catching of popup message "stop this running script" by executing a script with more than 5 000 000 instructions
    // TODO: allow to move element with : http://www.petercollingridge.co.uk/tutorials/svg/interactive/dragging/
    public partial class SVGViewer : UserControl
    {
        public string Svg
        {
            get { return _Svg; }
            set
            {
                if (_Svg == value)
                    return;
                UpdateMainPageContent(value, _ShowControls);
                _Svg = value;
            }
        }
        string _Svg;

        public bool ShowControls
        {
            get { return _ShowControls; }
            set
            {
                if (_ShowControls == value)
                    return;
                UpdateMainPageContent(_Svg, value);
                _ShowControls = value;
            }
        }
        bool _ShowControls = true;

        public event EventHandler Displayed;

        public SVGViewer()
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            ContextMenu_Init();
            webBrowser.DocumentCompleted += webBrowser_DocumentCompleted;
            webBrowser.IsWebBrowserContextMenuEnabled = false;
            webBrowser.ScriptErrorsSuppressed = true;
            webBrowser.NewWindow += WebBrowser_NewWindow;
        }

        private void WebBrowser_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        void UpdateMainPageContent(string svg, bool showControls)
        {
            if (svg != null)
            {
                //webBrowser.DocumentStream = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetPreamble().Concat(System.Text.Encoding.UTF8.GetBytes(GenerateMainPageContent(svg, showControls))).ToArray());
                _lastDocumentTextAssigned = GenerateMainPageContent(svg, showControls);
                webBrowser.DocumentText = _lastDocumentTextAssigned;
                //webBrowser.Document.Encoding = "UTF-8";
            }
        }
        string _lastDocumentTextAssigned;
        string GenerateMainPageContent(string svg, bool showControls)
        {
            return @"<?xml version=""1.0"" encoding=""UTF-8"" standalone=""no""?>
<!DOCTYPE HTML>
<html>
<head>
    <!-- Important, otherwhile js library are not correctly interpreted --> 
    <meta charset=""utf-8"" />    
    <meta http-equiv=""X-UA-Compatible"" content=""IE=edge,chrome=1"" />
    
	<script type=""text/javascript"">
" + GetjQuery1_11_0() + @"
    </script>
	<script type=""text/javascript"">
" + GetSvgPanZoom() + @"
    </script>
</head>
<body style=""height: 100vh; margin:0px;"" scroll=""no"">
    <div style=""width: 100%; height: 100%; border:0px"">
" + svg.Replace("<svg ", "<svg id=\"svg-id\" "
                       + "style=\"display: inline; width: inherit; min-width: inherit; max-width: inherit; height: inherit; min-height: inherit; max-height: inherit;\" ") + @"
</div>
    <script type=""text/javascript"">

// Wait for the dom to be ready (seems to be needed for large SVG)
document.addEventListener(""DOMContentLoaded"", function()
{ 
    $(function() 
    {
	    panZoomInstance = 	svgPanZoom('#svg-id',
						    {
							    zoomEnabled: true,
							    controlIconsEnabled: " + showControls.ToString().ToLowerInvariant() + @",
							    fit: true,
							    center: true,
							    minZoom: 0.01, // default is 0.5
                                maxZoom: 100 // default is 10
						    });

	    // zoom out
	    //panZoomInstance.zoom(0.2)

	    window.onresize = function()
	    {
		    panZoomInstance.resize(); // update SVG cached size and controls positions
		    //panZoomInstance.fit();
		    //panZoomInstance.center();
	    };

    });
});
</script>
</body>
</html>";
        }

        void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webBrowser.IsWebBrowserContextMenuEnabled = false;
            webBrowser.ScriptErrorsSuppressed = true;
            var doc = webBrowser.Document;
            if (doc != null)
                doc.Window.Error += Document_Window_Error;
            Displayed?.Invoke(this, EventArgs.Empty);
        }

        void Document_Window_Error(object sender, HtmlElementErrorEventArgs e)
        {
            e.Handled = true;
        }

        #region ContextMenu management

        public event EventHandler<PopupMenuShowingEventArgs> PopupMenuShowing;

        public class PopupMenuShowingEventArgs : CancelEventArgs
        {
            public PopupMenuShowingEventArgs(ContextMenu menu, Point point)
            {
                PopupMenu = menu;
                Point = point;
            }

            public ContextMenu PopupMenu { get; }
            public Point Point { get; set; }
        }

        void ContextMenu_Init()
        {
            AssignContextMenuHandler();
            webBrowser.DocumentCompleted += (_, __) => AssignContextMenuHandler();

            PopupMenuShowing += SVGViewer_PopupMenuShowing;
        }

        public void ShowInMemoForm(Func<string> getTextContent, string title)
        {
            var textContent = getTextContent();
            var mf = new Form() { Text = Text + " — " + title };
            var txt = new TextBox()
            {
                Text = textContent,
                ReadOnly = true,
                Multiline = true,
                Dock = DockStyle.Fill,
                MaxLength = 0, // Override the default limit of 32767 chars
            };
            mf.Controls.Add(txt);
            mf.ShowIcon = false;
            mf.ShowInTaskbar = false;
            mf.StartPosition = FormStartPosition.Manual;
            var ownerForm = FindForm();
            mf.SetBounds(ownerForm.Left + ownerForm.Width * 5 / 100 / 2, ownerForm.Top + ownerForm.Height * 5 / 100 / 2,
                         ownerForm.Width * 95 / 100, ownerForm.Height * 95 / 100);
            mf.Show(ownerForm);
        }

        void SVGViewer_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            e.PopupMenu.MenuItems.Add(new MenuItem("Search...",
                (_, __) => {
                    webBrowser.Focus();
                    SendKeys.Send("^f");
                }));
            e.PopupMenu.MenuItems.Add(new MenuItem("-"));
            e.PopupMenu.MenuItems.Add(new MenuItem("See SVG description",
                (_, __) => ShowInMemoForm(() => Svg, "SVG description"))
            {
            });
            e.PopupMenu.MenuItems.Add(new MenuItem("See page's source code",
                (_, __) => ShowInMemoForm(() => _lastDocumentTextAssigned, "Page's source code"))
            {
                Enabled = !string.IsNullOrWhiteSpace(_lastDocumentTextAssigned)
            });
        }

        void AssignContextMenuHandler()
        {
            var doc = webBrowser.Document;
            if (doc != null)
            {
                doc.MouseUp -= Document_MouseUp;
                doc.MouseUp += Document_MouseUp;
            }
        }

        private void Document_MouseUp(object sender, HtmlElementEventArgs e)
        {
            if (!e.MouseButtonsPressed.HasFlag(MouseButtons.Right))
                return;
            var ee = new PopupMenuShowingEventArgs(new ContextMenu(), e.ClientMousePosition);
            try
            {
                PopupMenuShowing?.Invoke(this, ee);
            }
            finally
            {
                if (ee.PopupMenu.MenuItems.Count > 0)
                {
                    // We must use this and not webBrowser, 
                    // otherwise the eventHandler of menu are not called
                    // (this is related with the fact webBrowser is an activeX control)
                    // e.ClientMousePosition seems to be a good value, maybe to convert to another referential later
                    ee.PopupMenu.Show(this, ee.Point);
                }
            }
        }
        #endregion
    }

}
