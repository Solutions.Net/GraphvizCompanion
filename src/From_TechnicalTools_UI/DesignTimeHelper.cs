﻿using System.ComponentModel;
using System.Diagnostics;


namespace TechnicalTools.UI
{
    public static class DesignTimeHelper
    {
        /// <summary>
        /// Cette methode sert à palier le default de fonctionnement de la propriété DesignMode, un peu foireuse, de Visual Studio 
        /// </summary>
        public static bool IsInDesignMode
        {
            [DebuggerHidden, DebuggerStepThrough]
            get
            {
                if (_isInDesignMode.HasValue)
                    return _isInDesignMode.Value;

                if (LicenseManager.UsageMode == LicenseUsageMode.Designtime)
                {
                    _isInDesignMode = true;
                    return true;
                }

                // UsageMode is wrong sometimes, so...
                try
                {
                    // Ugly but work very well. I never get a problem with this code.
                    using (var process = Process.GetCurrentProcess())
                        _isInDesignMode = process.ProcessName.ToLowerInvariant().Contains("devenv");
                }
                catch // It never happened yet :)
                {
                    // Can't know :( because of weird thing...
                    // We consider making release product working a priority
                    // So we use false value
                    if (Debugger.IsAttached) // Important ! Otherwise Break can make reelease app crash
                        Debugger.Break();
                    _isInDesignMode = false;
                }
                return _isInDesignMode.Value;
            }
        }
        static bool? _isInDesignMode;
    }
}
