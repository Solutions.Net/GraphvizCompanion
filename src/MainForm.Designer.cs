﻿namespace GraphvizCompanion
{
    partial class MainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConvertAndShow = new System.Windows.Forms.Button();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chkShowZoomControl = new System.Windows.Forms.CheckBox();
            this.chkAutoRefreshOnChange = new System.Windows.Forms.CheckBox();
            this.cmbLayoutEngine = new System.Windows.Forms.ComboBox();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabResult = new System.Windows.Forms.TabPage();
            this.svgViewer = new TechnicalTools.UI.DX.Controls.SVGViewer();
            this.tabError = new System.Windows.Forms.TabPage();
            this.txtErrors = new System.Windows.Forms.TextBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabs.SuspendLayout();
            this.tabResult.SuspendLayout();
            this.tabError.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConvertAndShow
            // 
            this.btnConvertAndShow.Location = new System.Drawing.Point(106, -1);
            this.btnConvertAndShow.Name = "btnConvertAndShow";
            this.btnConvertAndShow.Size = new System.Drawing.Size(114, 23);
            this.btnConvertAndShow.TabIndex = 1;
            this.btnConvertAndShow.Text = "Convert And Show";
            this.btnConvertAndShow.UseVisualStyleBackColor = true;
            this.btnConvertAndShow.Click += new System.EventHandler(this.btnConvertAndShow_Click);
            // 
            // txtInput
            // 
            this.txtInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtInput.Location = new System.Drawing.Point(0, 0);
            this.txtInput.Multiline = true;
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(327, 550);
            this.txtInput.TabIndex = 2;
            this.txtInput.Text = "digraph g\r\n{\r\n  TODO; \r\n  Done [label=\"Done!\"];\r\n\r\n  TODO -> Done [label=\"work!\"]" +
    ";\r\n}\r\n";
            this.txtInput.TextChanged += new System.EventHandler(this.txtInput_TextChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtInput);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.chkShowZoomControl);
            this.splitContainer1.Panel2.Controls.Add(this.chkAutoRefreshOnChange);
            this.splitContainer1.Panel2.Controls.Add(this.cmbLayoutEngine);
            this.splitContainer1.Panel2.Controls.Add(this.btnConvertAndShow);
            this.splitContainer1.Panel2.Controls.Add(this.tabs);
            this.splitContainer1.Size = new System.Drawing.Size(1018, 550);
            this.splitContainer1.SplitterDistance = 327;
            this.splitContainer1.TabIndex = 3;
            // 
            // chkShowZoomControl
            // 
            this.chkShowZoomControl.AutoSize = true;
            this.chkShowZoomControl.Checked = true;
            this.chkShowZoomControl.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowZoomControl.Location = new System.Drawing.Point(489, 3);
            this.chkShowZoomControl.Name = "chkShowZoomControl";
            this.chkShowZoomControl.Size = new System.Drawing.Size(119, 17);
            this.chkShowZoomControl.TabIndex = 4;
            this.chkShowZoomControl.Text = "Show Zoom Control";
            this.chkShowZoomControl.UseVisualStyleBackColor = true;
            this.chkShowZoomControl.CheckedChanged += new System.EventHandler(this.chkShowZoomControl_CheckedChanged);
            // 
            // chkAutoRefreshOnChange
            // 
            this.chkAutoRefreshOnChange.AutoSize = true;
            this.chkAutoRefreshOnChange.Checked = true;
            this.chkAutoRefreshOnChange.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoRefreshOnChange.Location = new System.Drawing.Point(346, 3);
            this.chkAutoRefreshOnChange.Name = "chkAutoRefreshOnChange";
            this.chkAutoRefreshOnChange.Size = new System.Drawing.Size(137, 17);
            this.chkAutoRefreshOnChange.TabIndex = 4;
            this.chkAutoRefreshOnChange.Text = "Auto refresh on change";
            this.chkAutoRefreshOnChange.UseVisualStyleBackColor = true;
            // 
            // cmbLayoutEngine
            // 
            this.cmbLayoutEngine.FormattingEnabled = true;
            this.cmbLayoutEngine.Location = new System.Drawing.Point(226, 0);
            this.cmbLayoutEngine.Name = "cmbLayoutEngine";
            this.cmbLayoutEngine.Size = new System.Drawing.Size(114, 21);
            this.cmbLayoutEngine.TabIndex = 5;
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tabResult);
            this.tabs.Controls.Add(this.tabError);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabs.Location = new System.Drawing.Point(0, 0);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(687, 550);
            this.tabs.TabIndex = 1;
            // 
            // tabResult
            // 
            this.tabResult.Controls.Add(this.svgViewer);
            this.tabResult.Location = new System.Drawing.Point(4, 22);
            this.tabResult.Name = "tabResult";
            this.tabResult.Padding = new System.Windows.Forms.Padding(3);
            this.tabResult.Size = new System.Drawing.Size(679, 524);
            this.tabResult.TabIndex = 1;
            this.tabResult.Text = "Result";
            // 
            // svgViewer
            // 
            this.svgViewer.BackColor = System.Drawing.Color.Red;
            this.svgViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.svgViewer.Location = new System.Drawing.Point(3, 3);
            this.svgViewer.Name = "svgViewer";
            this.svgViewer.ShowControls = true;
            this.svgViewer.Size = new System.Drawing.Size(673, 518);
            this.svgViewer.Svg = null;
            this.svgViewer.TabIndex = 0;
            // 
            // tabError
            // 
            this.tabError.Controls.Add(this.txtErrors);
            this.tabError.Location = new System.Drawing.Point(4, 22);
            this.tabError.Name = "tabError";
            this.tabError.Padding = new System.Windows.Forms.Padding(3);
            this.tabError.Size = new System.Drawing.Size(679, 524);
            this.tabError.TabIndex = 0;
            this.tabError.Text = "Errors";
            this.tabError.UseVisualStyleBackColor = true;
            // 
            // txtErrors
            // 
            this.txtErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtErrors.Location = new System.Drawing.Point(3, 3);
            this.txtErrors.Multiline = true;
            this.txtErrors.Name = "txtErrors";
            this.txtErrors.Size = new System.Drawing.Size(673, 518);
            this.txtErrors.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 550);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Graphviz Companion";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabs.ResumeLayout(false);
            this.tabResult.ResumeLayout(false);
            this.tabError.ResumeLayout(false);
            this.tabError.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.Controls.SVGViewer svgViewer;
        private System.Windows.Forms.Button btnConvertAndShow;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabError;
        private System.Windows.Forms.TabPage tabResult;
        private System.Windows.Forms.TextBox txtErrors;
        private System.Windows.Forms.CheckBox chkShowZoomControl;
        private System.Windows.Forms.ComboBox cmbLayoutEngine;
        private System.Windows.Forms.CheckBox chkAutoRefreshOnChange;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

