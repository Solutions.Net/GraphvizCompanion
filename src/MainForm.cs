﻿using System;
using System.Windows.Forms;

using ApplicationBase.Business.GraphVisualization.Graphviz;


namespace GraphvizCompanion
{
    // TODO: See dynamic moving code from https://stackoverflow.com/a/46272057
    // Alternative in WPF: https://www.codeproject.com/Articles/18870/Dot2WPF-a-WPF-control-for-viewing-Dot-graphs
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            txtInput.MaxLength = 0; // Override the default limit of 32767 chars
            txtInput.ScrollBars = ScrollBars.Both;
            txtInput.WordWrap = false;
            txtErrors.MaxLength = 0;
            svgViewer.PopupMenuShowing += SvgViewer_PopupMenuShowing;
        }

        private void Form1_Load(object sender, EventArgs _)
        {
            chkShowZoomControl.Checked = svgViewer.ShowControls;

            foreach (eLayoutEngine le in Enum.GetValues(typeof(eLayoutEngine)))
                cmbLayoutEngine.Items.Add(le);
            cmbLayoutEngine.SelectedItem = cmbLayoutEngine.Items[0];
            cmbLayoutEngine.DropDownStyle = ComboBoxStyle.DropDownList; // Make kind of readonly
            cmbLayoutEngine.KeyDown += (__, e) => e.SuppressKeyPress = true; // make truly readonly

            if (!new GraphVizGenerator().IsGraphVizInstalled)
                MessageBox.Show(@"Graphviz not detected! File .\lib\graphviz\dot.exe does not exist!");

            txtInput.SelectionStart = 50;
            BeginInvoke((MethodInvoker)btnConvertAndShow.PerformClick);
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            if (!chkAutoRefreshOnChange.Checked)
                return;
            var textBefore = _textBefore;
            _textBefore = txtInput.Text;
            if (_textBefore.Split('\n').Length - textBefore.Split('\n').Length > 100)
            {
                // Too much code we let user choose if he really want to refresh and enabled back the option
                // because it can freeze UI a long time
                chkAutoRefreshOnChange.Checked = false;
            }
            else
            {
                _resultToRefresh = true;

                // Note : When user replace selected text, two events occur : remove of selected text, then insert
                // So we postpone to make multiple event only one.
                // Especially if the state inf input text between the two event is bad. it cause flickering for user
                BeginInvoke((MethodInvoker)(() =>
                {
                    if (_resultToRefresh)
                    {
                        _resultToRefresh = false;
                        ConvertAndShow();
                    }
                }));
            }
        }
        string _textBefore = string.Empty;
        bool _resultToRefresh;

        private void btnConvertAndShow_Click(object sender, EventArgs _)
        {
            ConvertAndShow();
        }
        private void chkShowZoomControl_CheckedChanged(object sender, EventArgs e)
        {
            ConvertAndShow();
        }

        void ConvertAndShow()
        {
            var input = txtInput.Text;
            var layoutEngine = (eLayoutEngine)cmbLayoutEngine.SelectedItem;
            var showZoomControl = chkShowZoomControl.Checked;
            try
            {
                var generator = new GraphVizGenerator();
                var svg = generator.Run(layoutEngine, eOutputType.svg, input);

                _lastSuccessfulInput = input;
                SetSelectedTab(tabResult);
                svgViewer.ShowControls = showZoomControl;
                svgViewer.Svg = svg;
            }
            catch (GraphVizGenerator.ConversionException ex)
            {
                txtErrors.Text = ex.Message;
                SetSelectedTab(tabError);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Unexpected error :-(");
            }
        }
        string _lastSuccessfulInput;

        void SetSelectedTab(TabPage tabPage)
        {
            var inputFocused = txtInput.Focused;
            if (ActiveControl != null)
            {
                tabs.SelectedTab = tabPage;
                txtInput.Focus();
            }
        }

        void SvgViewer_PopupMenuShowing(object sender, TechnicalTools.UI.DX.Controls.SVGViewer.PopupMenuShowingEventArgs e)
        {
            e.PopupMenu.MenuItems.Add(new MenuItem("Export To Png", (_, __) =>
            {
                ExportToImage("Png", eOutputType.png, _lastSuccessfulInput);
            })
            { Enabled = _lastSuccessfulInput != null });
            e.PopupMenu.MenuItems.Add(new MenuItem("Export To Jpg", (_, __) =>
            {
                ExportToImage("Jpg", eOutputType.jpg, _lastSuccessfulInput);
            })
            { Enabled = _lastSuccessfulInput != null });
        }

        void ExportToImage(string extension, eOutputType ext, string input)
        {
            var layoutEngine = (eLayoutEngine)cmbLayoutEngine.SelectedItem;
            try
            {
                saveFileDialog.Filter = extension + " files|*." + extension.ToLower();
                if (saveFileDialog.ShowDialog() != DialogResult.OK)
                    return;

                var generator = new GraphVizGenerator();
                generator.Run(layoutEngine, ext, input, saveFileDialog.FileName);
                MessageBox.Show(this, "Export done sucessfuly!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Unexpected error :-(");
            }
        }
    }
}
