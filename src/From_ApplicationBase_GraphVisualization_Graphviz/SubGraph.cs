﻿using System;
using System.Text;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class SubGraph : BaseGraph
    {
        public SubGraph(BaseGraph owner)
             : base(owner)
        {
            GraphVizGraphName = "cluster_" + GraphVizGraphName;
        }

        #region Writing out to dot language

        public override void ToDot(StringBuilder sb)
        {
            sb.AppendLine("subgraph " + GraphVizGraphName);
            sb.AppendLine("{");
            ++IndentationLevel;
            ToDotContent(sb);
            --IndentationLevel;
            sb.AppendLine("}");
        }
        public override void ToDotContent(StringBuilder sb)
        {

        }

        #endregion Writing out to dot language
    }
}
