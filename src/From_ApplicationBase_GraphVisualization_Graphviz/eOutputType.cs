﻿using System;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    // See here https://graphviz.org/doc/info/output.html
    public enum eOutputType
    {
        bmp,
        canon,
        dot,
        gv,
        xdot,
        xdot1_2,
        xdot1_4,
        cgimage,
        cmap,
        eps,
        exr,
        fig,
        gd,
        gd2,
        gif,
        gtk,
        ico,
        imap,
        cmapx,
        imap_np,
        cmapx_np,
        ismap,
        jp2,
        jpg,
        jpeg,
        jpe,
        json,
        json0,
        dot_json,
        xdot_json,
        pct,
        pict,
        pdf,
        pic,
        plain,
        plain_ext,
        png,
        pov,
        ps,
        ps2,
        psd,
        sgi,
        svg,
        svgz,
        tga,
        tif,
        tiff,
        tk,
        vml,
        vmlz,
        vrml,
        wbmp,
        webp,
        xlib,
        x11,
    }

    public static class eOutputType_Extensions
    {
        public static string ToGraphvizArgumentValue(this eOutputType t)
        {
            switch (t)
            {
                case eOutputType.plain_ext: return "plain-ext";
                case eOutputType.xdot1_2: return "xdot1.2";
                case eOutputType.xdot1_4: return "xdot1.4";
                default: return t.ToString().ToLowerInvariant();
            }
        }
    }
}
