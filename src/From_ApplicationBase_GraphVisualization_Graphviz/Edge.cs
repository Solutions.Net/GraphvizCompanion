﻿using System;
using System.Drawing;
using System.Text;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class Edge : EdgePropertiesContainer
    {
        public BaseGraph Parent          { get; }
        public int       Id              { get; }

        public Node      From            { get; set; }
        public Node      To              { get; set; }

        public Edge(BaseGraph parent,Node from, Node to)
        {
            Parent = parent;
            Id = System.Threading.Interlocked.Increment(ref parent.RootGraph._edgeIdSeed);
            From = from;
            To = to;
        }

        public void Remove() { Parent.Remove(this); }

        public virtual void Write(StringBuilder sb)
        {
            sb.Append(From.GraphVizNodeName + " -> " + To.GraphVizNodeName);
            WriteProperties(sb);
            sb.AppendLine(";");
        }
    }
}
