﻿using System;
using System.Text;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class NodeDefaultProperties : NodePropertiesContainer
    {
        public virtual void Write(StringBuilder sb)
        {
            if (PropertiesHaveDefaultValues)
                return;
            sb.Append("node ");
            WriteProperties(sb);
            sb.AppendLine(";");
        }
    }
}
