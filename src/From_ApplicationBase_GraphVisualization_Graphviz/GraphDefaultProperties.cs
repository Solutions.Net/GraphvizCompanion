﻿using System;
using System.Text;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class GraphDefaultProperties : GraphPropertiesContainer
    {
        public virtual void Write(StringBuilder sb)
        {
            if (PropertiesHaveDefaultValues)
                return;
            sb.Append("graph ");
            WriteProperties(sb);
            sb.AppendLine(";");
        }

    }
}
