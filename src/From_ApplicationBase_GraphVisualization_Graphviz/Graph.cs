﻿using System;
using System.Collections.Generic;
using System.Text;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class Graph : BaseGraph
    {
        public bool IsDirectedGraph { get; set; } = true;

        public Graph()
            : base(null)
        {
            GraphVizGraphName = "g";
        }

        #region Writing out to dot language

        public string ToDot()
        {
            var sb = new StringBuilder();
            ToDot(sb);
            return sb.ToString();
        }
        public override void ToDot(StringBuilder sb)
        {
            sb.AppendLine((IsDirectedGraph ? "di" : "") + "graph " + GraphVizGraphName);
            sb.AppendLine("{");
            ++IndentationLevel;
            ToDotContent(sb);
            --IndentationLevel;
            sb.AppendLine("}");
        }

        #endregion Writing out to dot language
    }
}
