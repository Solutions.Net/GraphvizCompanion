﻿using System;
using System.IO;
using System.Text;

using TechnicalTools.Tools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    /// <summary>
    /// Helper class to create graph.
    /// Example with a graph described as string. You needs graphviz library to be in .\lib\graphviz"):
    /// <code>
    /// new GraphVizGenerator().Run(eLayoutEngine.Dot, eOutputType.png, "digraph g { TODO; Done [label=\"Done!\"]; TODO -> Done [label=\"work!\"]; }")
    /// </code>
    /// </summary>
    public class GraphVizGenerator
    {
        /// <summary> Tell if this class can work </summary>
        public bool IsGraphVizInstalled
        {
            get
            {
                return File.Exists(DotExe);
            }
        }
        const string DotExe = @"graphviz\dot.exe";
        /// <summary>
        /// <see cref="Run(eLayoutEngine, eOutputType, string, string)"/>
        /// </summary>
        public string Run(eLayoutEngine engine, eOutputType outputType, Graph graph, string outputFileName = null)
        {
            return Run(engine, outputType, graph.ToDot(), outputFileName);
        }
        /// <summary>
        /// Simple wrapper to convert dot graph description to image or other format
        /// </summary>
        /// <param name="engine">The layout engine. Dot being the most used and recommended one</param>
        /// <param name="outputType">Png & Jpg are a good start. Svg (html text) is a good chocie to get clean display with UI interaction (zooming, etc)</param>
        /// <param name="graphDescription">Description of graph like "digraph g {a -&gt; b; }"</param>
        /// <param name="outputFileName">Optional file path where to put the result. Recommended for image typere</param>
        /// <returns>graph if outputFileName is null</returns>
        public string Run(eLayoutEngine engine, eOutputType outputType, string graphDescription, string outputFileName = null)
        {
            if (outputFileName != null)
                File.Exists(outputFileName); // Delegate checks for path

            var args = (engine != eLayoutEngine.Dot ? " -K" + engine.ToGraphvizArgumentValue() : "")
                     + " -T" + outputType.ToGraphvizArgumentValue()
                     + (outputFileName != null ? " -o \"" + outputFileName + "\"" : "")
                     ;// + " -Gcharset=latin1";

            var rp = new RedirectedProcess(DotExe, args);
            var log = new MemoryStream();
            var err = new MemoryStream();
            var input = new MemoryStream(Encoding.UTF8.GetBytes(graphDescription));
            var exitCode = rp.Execute(log, err, input);
            if (exitCode != 0)
            {
                var errors = Encoding.Default.GetString(err.ToArray());
                throw new ConversionException(errors);
            }
            var res = Encoding.UTF8.GetString(log.ToArray());
            return res;
        }
        public class ConversionException : Exception
        {
            public ConversionException(string msg) : base(msg) { }
        }
    }
}
