﻿using System;
using System.Drawing;
using System.Text;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class NodePropertiesContainer
    {
        public string Label           { get; set; }
        public Color? BackgroundColor { get; set; }
        public Color? Color           { get; set; }
        public bool   Invisible       { get; set; }

        /// <summary> Return true is all values are the default ones </summary>
        public bool PropertiesHaveDefaultValues
        {
            get
            {
                return string.IsNullOrEmpty(Label)
                    && (BackgroundColor == null || BackgroundColor.Value.ToHexString() == "#00000")
                    && (Color == null || Color.Value.ToHexString() == "#00000")
                    && !Invisible;
            }
        }

        protected internal virtual void WriteProperties(StringBuilder sb)
        {
            sb.Append(" [");
            string sep = "";
            if (!string.IsNullOrEmpty(Label))
            {
                sb.Append(sep + "label=\"" + BaseGraph.EscapeRawLabel(Label) + "\"");
                sep = ", ";
            }
            if (Color.HasValue)
            {
                sb.Append(sep + "color=\"" + Color.Value.ToHexString() + "\"");
                sep = ", ";
            }
            if (BackgroundColor.HasValue)
            {
                sb.Append(sep + "fillcolor=\"" + BackgroundColor.Value.ToHexString() + "\", style=filled");
                sep = ", ";
            }
            if (Invisible)
            {
                sb.Append(sep + "style=invisible");
                sep = ", ";
            }
            sb.Append("]");
        }
    }
}
