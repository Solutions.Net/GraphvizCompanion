﻿using System;
using System.Collections.Generic;
using System.Text;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public abstract class BaseGraph : GraphPropertiesContainer
    {
        public BaseGraph RootGraph         { get { return Parent == null ? this : Parent.RootGraph; } }
        public BaseGraph Parent            { get; }
        public int       Id                { get; }
        public string    GraphVizGraphName { get { return _GraphVizNodeName ?? "n" + Id.ToStringInvariant(); } set { _GraphVizNodeName = value; } } string _GraphVizNodeName;


        public IReadOnlyList<Node>     Nodes     { get { return _nodes; } } readonly List<Node> _nodes = new List<Node>();
        public IReadOnlyList<Edge>     Edges     { get { return _edges; } } readonly List<Edge> _edges = new List<Edge>();
        public IReadOnlyList<SubGraph> SubGraphs { get { return _subGraphs; } } readonly List<SubGraph> _subGraphs = new List<SubGraph>();

        public NodeDefaultProperties  NodeDefaultProperties  { get; set; } = new NodeDefaultProperties();
        public EdgeDefaultProperties  EdgeDefaultProperties  { get; set; } = new EdgeDefaultProperties();

        protected BaseGraph(BaseGraph parent)
        {
            if (parent == null && !(this is Graph))
                throw new ArgumentNullException(nameof(SubGraph) + " needs a parent!", nameof(parent));
            Parent = parent;
            Id = System.Threading.Interlocked.Increment(ref RootGraph._subGraphsIdSeed);
        }
        internal int _nodeIdSeed = 0;
        internal int _edgeIdSeed = 0;
        internal int _subGraphsIdSeed = 0;


        public Node NewNode()
        {
            var n = CreateNode(this);
            _nodes.Add(n);
            return n;
        }
        protected virtual Node CreateNode(BaseGraph parent)
        {
            return new Node(parent);
        }
        public virtual void Remove(Node n)
        {
            foreach (var e in _edges)
                if (e.From == n || e.To == n)
                    Remove(e);
            _nodes.Remove(n);
        }

        public Edge NewEdge(Node from, Node to)
        {
            var e = CreateEdge(this, from, to);
            _edges.Add(e);
            return e;
        }
        protected virtual Edge CreateEdge(BaseGraph owner, Node from, Node to)
        {
            return new Edge(owner, from, to);
        }
        public virtual void Remove(Edge e)
        {
            _edges.Remove(e);
        }


        public SubGraph NewSubGraph(Node from, Node to)
        {
            var g = CreateSubGraph(this);
            _subGraphs.Add(g);
            return g;
        }
        protected virtual SubGraph CreateSubGraph(BaseGraph owner)
        {
            return new SubGraph(owner);
        }
        public virtual void Remove(SubGraph g)
        {
            _subGraphs.Remove(g);
        }


        bool IsSubGraph(SubGraph g)
        {
            if (g == null)
                throw new ArgumentNullException(nameof(g));
            if (g == this)
                return false;
            var bg = g.Parent;
            while (bg != null && bg != this)
                bg = bg.Parent;
            return bg != null;
        }

        bool IsSubNode(Node n)
        {
            return n != null 
                && n.Parent is SubGraph sg 
                && IsSubGraph(sg);
        }

        #region Writing out to dot language

        protected int    IndentationLevel { get; set; }
        protected string IndentationElement = "\t";
        protected string Indentation
        {
            get
            {
                var indent = string.Empty;
                for (int i = 0; i < IndentationLevel; ++i)
                    indent += IndentationElement;
                return indent;
            }
        }

        internal static string EscapeRawLabel(string rawLabel)
        {
            return rawLabel.Replace("\"", "\\\"")
                           .Replace(Environment.NewLine, "\\n");
        }

        /// <summary>
        /// Write the whole description in dot format.
        /// Basically it write the wrapper "graph { ... }" and call ToDotContent for the content (the 3 dots)
        /// </summary>
        /// <param name="sb"></param>
        public abstract void ToDot(StringBuilder sb);
        /// <summary>
        /// Write the contnet between { and }
        /// </summary>
        /// <param name="sb"></param>
        public virtual void ToDotContent(StringBuilder sb)
        {
            if (!PropertiesHaveDefaultValues)
            {
                sb.Append("graph ");
                base.WriteProperties(sb);
                sb.AppendLine(";");
            }
            if (!NodeDefaultProperties.PropertiesHaveDefaultValues)
                NodeDefaultProperties.Write(sb);
            if (!EdgeDefaultProperties.PropertiesHaveDefaultValues)
                EdgeDefaultProperties.Write(sb);

            foreach (var node in Nodes)
                if (!IsSubNode(node))
                    node.Write(sb);

            foreach (var sg in SubGraphs)
                sg.ToDot(sb);

            foreach (var e in Edges)
                e.Write(sb);
        }

        #endregion Writing out to dot language
    }
}
