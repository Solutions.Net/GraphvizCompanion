﻿using System;
using System.Text;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class EdgeDefaultProperties : EdgePropertiesContainer
    {
        public virtual void Write(StringBuilder sb)
        {
            if (PropertiesHaveDefaultValues)
                return;
            sb.Append("edge ");
            WriteProperties(sb);
            sb.AppendLine(";");
        }
    }
}
