﻿using System;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    // See here https://graphviz.org/doc/info/output.html
    public enum eLayoutEngine
    {
        Dot,
        Circo,
        Twopi,
        Neato,
        Fdp,
        Sfdp,
        Osage,
        Patchwork,
    }

    public static class eRenderingEngine_Extensions
    {
        // often used with prefix "-K" as argument to specify layout
        public static string ToGraphvizArgumentValue(this eLayoutEngine re)
        {
            return re.ToString().ToLowerInvariant();
        }
    }
}
