﻿using System;
using System.Drawing;
using System.Text;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class EdgePropertiesContainer
    {
        public string Label { get; set; }
        public Color? Color { get; set; }
        public bool Invisible { get; set; }

        /// <summary> Return true is all values are the default ones </summary>
        public bool PropertiesHaveDefaultValues
        {
            get
            {
                return string.IsNullOrEmpty(Label)
                    && (Color == null || Color.Value.ToHexString() == "#00000")
                    && !Invisible;
            }
        }

        protected internal virtual void WriteProperties(StringBuilder sb)
        {
            sb.Append(" [");
            string sep = "";
            if (!string.IsNullOrEmpty(Label))
            {
                sb.Append(sep + "label=\"" + BaseGraph.EscapeRawLabel(Label) + "\"");
                sep = ", ";
            }
            if (Color.HasValue)
            {
                sb.Append(sep + "color=\"" + Color.Value.ToHexString() + "\"");
                sep = ", ";
            }
            if (Invisible)
            {
                sb.Append(sep + "style=invisible, arrowhead=no");
                sep = ", ";
            }
            sb.Append("]");
        }
    }
}
