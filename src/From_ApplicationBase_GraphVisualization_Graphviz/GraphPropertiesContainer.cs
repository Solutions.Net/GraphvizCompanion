﻿using System;
using System.Collections.Generic;
using System.Text;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class GraphPropertiesContainer
    {
        public string    Label     { get; set; }

        public decimal?  FontSize  { get; set; }
        public char?     LabelLoc  { get; set; } // "t" forexample
        public string    Splines   { get; set; } // "true" (default) | "ortho"
        public bool?     Overlap   { get; set; }
        public string    RankDir   { get; set; }
        public string    Ratio     { get; set; }

        /// <summary> Return true is all values are the default ones </summary>
        public bool PropertiesHaveDefaultValues
        {
            get
            {
                return FontSize == null 
                    && LabelLoc == null
                    && string.IsNullOrEmpty(Splines)
                    && Overlap == null
                    && string.IsNullOrEmpty(RankDir)
                    && string.IsNullOrEmpty(Ratio);
            }
        }

        protected internal virtual void WriteProperties(StringBuilder sb)
        {
            sb.Append(" [");
            string sep = "";
            if (!string.IsNullOrEmpty(Label))
            {
                sb.Append(sep + "label=\"" + BaseGraph.EscapeRawLabel(Label) + "\"");
                sep = ", ";
            }
            if (LabelLoc != null)
            {
                sb.Append(sep + "labelloc=\"" + LabelLoc + "\"");
                sep = ", ";
            }
            if (!string.IsNullOrEmpty(Splines))
            {
                sb.Append(sep + "splines=\"" + BaseGraph.EscapeRawLabel(Splines) + "\"");
                sep = ", ";
            }
            if (Overlap.HasValue)
            {
                sb.Append(sep + "overlap=\"" + Overlap.Value.ToString().ToLowerInvariant() + "\"");
                sep = ", ";
            }
            if (!string.IsNullOrEmpty(RankDir))
            {
                sb.Append(sep + "rankdir=\"" + BaseGraph.EscapeRawLabel(RankDir) + "\"");
                sep = ", ";
            }
            if (!string.IsNullOrEmpty(Ratio))
            {
                sb.Append(sep + "ratio=\"" + BaseGraph.EscapeRawLabel(Ratio) + "\"");
                sep = ", ";
            }
            sb.Append("]");
        }
    }
}
