﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using TechnicalTools;


namespace ApplicationBase.Business.GraphVisualization.Graphviz
{
    public class Node : NodePropertiesContainer
    {
        public BaseGraph Parent { get; }
        public int Id { get; }
        public string GraphVizNodeName { get { return _GraphVizNodeName ?? "n" + Id.ToStringInvariant(); } set { _GraphVizNodeName = value; } } string _GraphVizNodeName;



        /// <summary> Return edges where this is the source of edge.</summary>
        public IEnumerable<Edge> EdgesStarting { get { return Parent.Edges.Where(e => e.From == this); } }
        /// <summary> Return edges where this is the source of edge.</summary>
        public IEnumerable<Edge> EdgesArriving { get { return Parent.Edges.Where(e => e.To == this); } }
        /// <summary> Return nodes targeted by this node, though edges.</summary>
        public IEnumerable<Node> NodesTargeting { get { return Parent.Edges.Where(e => e.From == this).Select(e => e.To); } }
        /// <summary> Return nodes pointing to "this".</summary>
        public IEnumerable<Node> NodesSourcing { get { return Parent.Edges.Where(e => e.To == this).Select(e => e.From); } }

        public Node(BaseGraph parent)
        {
            Parent = parent;
            Id = System.Threading.Interlocked.Increment(ref parent.RootGraph._nodeIdSeed);
        }

        public void Remove() { Parent.Remove(this); }

        public virtual void Write(StringBuilder sb)
        {
            sb.Append(GraphVizNodeName);
            WriteProperties(sb);
            sb.AppendLine(";");
        }
    }
}
