﻿using System;
using System.Drawing;


namespace TechnicalTools
{
    public static class Color_Extensions
    {
        public static string ToHexString(this Color c) => $"#{c.R:X2}{c.G:X2}{c.B:X2}";
    }

}
