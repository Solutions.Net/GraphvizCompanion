﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;


namespace TechnicalTools.Tools
{
    /// <summary>
    /// This class handles redirections of streams when running a process.
    /// <example>
    /// This code allow you to proxyfy a program and tee its output
    /// <code>
    /// var rp = new RedirectedProcess(..., ..., ...);
    /// var log = new MemoryStream();
    /// var err = new MemoryStream();
    /// rp.Execute(Console.OpenStandardOutput(), Console.OpenStandardError(), Console.OpenStandardInput(), log, err);
    /// </code>
    /// </example>
    /// <example>
    /// This code allow you to execute and get all output in some stream
    /// <code>
    /// var rp = new RedirectedProcess(..., ..., ...);
    /// var log = new MemoryStream();
    /// var err = new MemoryStream();
    /// rp.Execute(log, err);
    /// </code>
    /// </example>
    /// </summary>
    public class RedirectedProcess
    {
        public DateTime? StartUtc { get; private set; }
        public DateTime? EndUtc { get; private set; }
        public string ProcessPath { get { return _psi.FileName; } }
        public string Arguments { get { return _psi.Arguments; } }
        public string WorkingDirectory { get { return _psi.WorkingDirectory; } }

        public RedirectedProcess(string processPath, string allArguments, string workingDir = null)
        {
            _psi = new ProcessStartInfo();
            if (workingDir != null)
                _psi.WorkingDirectory = workingDir;
            _psi.FileName = processPath;
            _psi.Arguments = allArguments;
        }
        readonly ProcessStartInfo _psi;

        public int Execute(Stream output, Stream error, Stream input = null)
        {
            return Execute(output, error, input, null, null);
        }
        public int Execute(Stream output, Stream error, Stream input, Stream teeOutput, Stream teeError, Stream teeInput = null)
        {
            // from http://www.c-sharpcorner.com/article/redirecting-standard-inputoutput-using-the-process-class/
            _psi.UseShellExecute = false;
            _psi.CreateNoWindow = true; // only taken in account if UseShellExecute is false
            _psi.WindowStyle = ProcessWindowStyle.Hidden;

            _psi.RedirectStandardInput = true;
            _psi.RedirectStandardOutput = true;
            _psi.RedirectStandardError = true;

            Process p = new Process();
            p.StartInfo = _psi;
            p.EnableRaisingEvents = true; // https://msdn.microsoft.com/en-us/library/system.diagnostics.process.enableraisingevents(v=vs.110).aspx
                                          // TODO : https://stackoverflow.com/questions/33716580/process-output-redirection-on-a-single-console-line

            // Code inspired from https://stackoverflow.com/a/30517342
            // Depending on your application you may either prioritize the IO or the exact opposite
            const ThreadPriority ioPriority = ThreadPriority.Highest;

            var threadsCanRun = new Semaphore(initialCount: 0, maximumCount: 3);
            var outputThread = new Thread(_ => { threadsCanRun.WaitOne(); Redirect(p.StandardOutput.BaseStream, output, teeOutput); }) { Name = "ChildIO Output", Priority = ioPriority };
            outputThread.IsBackground = true;
            var errorThread = new Thread(_ => { threadsCanRun.WaitOne(); Redirect(p.StandardError.BaseStream, error, teeError); }) { Name = "ChildIO Error", Priority = ioPriority };
            errorThread.IsBackground = true;
            var inputThread = new Thread(_ => { threadsCanRun.WaitOne(); Redirect(input, p.StandardInput.BaseStream, teeInput); }) { Name = "ChildIO Input", Priority = ioPriority };
            inputThread.IsBackground = true;
            // Start the IO threads. Must be after p.Start()
            outputThread.Start(null);
            errorThread.Start(null);
            if (input != null)
                inputThread.Start(null);

            // Set as background threads (will automatically stop when application ends)


            EndUtc = null;
            StartUtc = DateTime.UtcNow;
            p.Start();

            threadsCanRun.Release(3);

            p.WaitForExit(); // flush the output buffer
            EndUtc = DateTime.UtcNow;

            return p.ExitCode;
        }


        /// <summary>
        /// Continuously copies data from one stream to the other.
        /// </summary>
        /// <param name="instream">The input stream.</param>
        /// <param name="outstream">The output stream.</param>
        static void Redirect(Stream instream, Stream outstream, Stream tee = null)
        {
            byte[] buffer = new byte[4096];
            int len;
            while ((len = instream.Read(buffer, 0, buffer.Length)) > 0)
            {
                if (tee != null)
                    lock (tee)
                    {
                        tee.Write(buffer, 0, len);
                        tee.Flush();
                    }
                outstream.Write(buffer, 0, len);
                outstream.Flush();
            }
            outstream.Close();
        }
    }
}
